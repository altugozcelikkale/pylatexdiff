# -*- mode: python -*-

# Add Gooey specific resource files into the binary distribution
import os
from pkgutil import get_loader
gooey_base_dir = os.path.split(get_loader('gooey').path)[0]
added_files = []
for subdir in ['languages', 'images']:
    added_files.append((os.path.join(gooey_base_dir, subdir), os.path.join('gooey', subdir)))

block_cipher = None


a = Analysis(['pylatexdiff.py'],
             pathex=['C:\\Seafile\\Altug\\Yazilim\\Python\\Projects\\pyLatexDiff'],
             binaries=[],
             datas=added_files,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='pylatexdiff',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False )
