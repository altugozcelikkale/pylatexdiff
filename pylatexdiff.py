import os
from argparse import ArgumentParser
from plumbum import local
from gooey import Gooey, GooeyParser


@Gooey(program_name="Python Latexdiff GUI")
def main():

    parser = GooeyParser(description="A wrapper around latexdiff command line interface.") # See latexdiff manual: http://mirror.utexas.edu/ctan/support/latexdiff/doc/latexdiff-man.pdf for full explanation of arguments.
    # Positional Arguments
    parser.add_argument("original_file", metavar="Original File", type=str, help="the original tex file", widget="FileChooser")
    parser.add_argument("revised_file", metavar="Revised File", type=str, help="the revised tex file", widget="FileChooser")

    # Optional Arguments
    parser.add_argument("-o", metavar="Output File", dest='output_file', help="Provide the path of the output file. Defaults to 'diff.tex in the original file's directory'", widget="FileChooser")
    parser.add_argument("-t", metavar="Markup Style (-t)", dest='markup_style', help="Select the style by which text revisions will be displayed", \
                        choices=['UNDERLINE', 'CTRADITIONAL', 'TRADITIONAL', 'CFONT', 'FONTSTRIKE', 'INVISIBLE', \
                                 'CHANGEBAR', 'CCHANGEBAR', 'CULINECHBAR', 'BOLD', 'PDFCOMMENT'], \
                        default="UNDERLINE")
    # 'CFONTCBHBAR' not implemented

    parser.add_argument("--flatten", metavar="Flatten",
                        help="Replace \input and \include commands within body \n by the content ofthe files in their argument.", \
                        action="store_true")
    parser.add_argument("--exclude-textcmd", metavar="Exclude Text Command", dest="exclude_textcmd", \
                        help="I don't really know what this option does, but if your diff file doesn't compile,\nprovide the name of the commands, without the \\, where the error occurs." \
                        )
    args = parser.parse_args()

    original_file_path = args.original_file
    revised_file_path = args.revised_file

    if args.output_file is None:
        output_file_path = os.path.join(os.path.split(original_file_path)[0], 'diff.tex')
    else:
        output_file_path = args.output_file

    # Built up command arguments:
    latexdiff = local['latexdiff']
    cmd_arguments = []
    cmd_arguments.extend(['-t', args.markup_style])
    if args.flatten:
        cmd_arguments.append('--flatten')
    if args.exclude_textcmd != '':
        cmd_arguments.append('--exclude-textcmd={!s}'.format(args.exclude_textcmd))
    cmd_arguments.append(original_file_path)
    cmd_arguments.append(revised_file_path)

    print('\nRunning latexdiff with the following command line arguments:\n')
    for item in cmd_arguments:
        print('\t{!s}'.format(item))

    retcode, stdout, strerr =latexdiff.run(tuple(cmd_arguments), retcode=0)
    with open(output_file_path, 'w', newline='') as fid:
        fid.write(stdout)
    print("\nAnnotated tex file is created: {!s}".format(output_file_path))

if __name__ == '__main__':
    main()
    print("\nExecution completed")